import 'package:app_ventasmk/src/pages/details_global.dart';
import 'package:app_ventasmk/src/pages/diary_sale.dart';
import 'package:app_ventasmk/src/pages/home_page.dart';
import 'package:bloc/bloc.dart';

enum NavigationEvents { HomePageClickedEvent, DailySaleGlobalPageEvent, DetailsGlobalSale}

abstract class NavigationStates {}

class NavigationBloc extends Bloc<NavigationEvents, NavigationStates> {
  @override
  NavigationStates get initialState => HomePage();

  @override
  Stream<NavigationStates> mapEventToState(NavigationEvents event ) async* {
    switch (event) {
      case NavigationEvents.HomePageClickedEvent:
        yield HomePage();
        break;
      case NavigationEvents.DailySaleGlobalPageEvent:
        yield DiarySaleGlobal();
        break;
      case NavigationEvents.DetailsGlobalSale:
        yield GlobalPage();
        break;

      // case NavigationEvents.MyOrdersClickedEvent:
      //   yield MyOrdersPage();
      //   break;
    }
  }
}
