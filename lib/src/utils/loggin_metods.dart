import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future logOut(BuildContext context) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  sharedPreferences.getKeys();
  for (String _key in sharedPreferences.getKeys()) {
    if (_key == "access_token") {
      sharedPreferences.remove(_key);
      Navigator.pushReplacementNamed(context, 'login');
    }
  }
}



