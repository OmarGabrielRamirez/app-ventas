import 'package:flutter/material.dart';


class MenuItem extends StatelessWidget {
  final IconData icon;
  final String title;
  final Color color;
  final Function onTap;

  const MenuItem({Key key, this.icon, this.title, this.color,this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Padding(
      padding: const EdgeInsets.all(20),
      child: Row(children: <Widget>[
        Icon(
          icon,
          color: Colors.white,
          size: 27,
        ),
        SizedBox(
          width: 20,
        ),
        Text(
          title,
          style: TextStyle(
              fontWeight: FontWeight.w300,
              fontFamily: 'Lato',
              fontSize: 17,
              color: Colors.white),
        ),
      ]),
    ),
    );
  }
}
