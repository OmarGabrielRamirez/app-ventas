import 'package:app_ventasmk/src/bloc.navigation_menu/bloc_navigation.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:app_ventasmk/src/utils/loggin_metods.dart';
import 'package:app_ventasmk/src/sidebar/menu_item.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:status_alert/status_alert.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:intl/intl.dart';
import 'dart:core';
import 'dart:async';

class SideBar extends StatefulWidget {
  @override
  _sideBarState createState() => _sideBarState();
}

class _sideBarState extends State<SideBar>
    with SingleTickerProviderStateMixin<SideBar> {
  String _month = "", _year = "";
  int _monthNumber = 0;
  AnimationController _animationController;
  StreamController<bool> isSideBarOpenedStreamController;
  Stream<bool> isSideBarOpenedStream;
  StreamSink<bool> isSideBarOpenedSink;

  final _animationDuration = const Duration(milliseconds: 100);

  @override
  void initState() {
    super.initState();
    setState(() {
      var date = DateTime.now();
      var formatter = new DateFormat('MM');
      var formatterYear = new DateFormat('yyyy');
      String monthDate = formatter.format(date);
      String yearDate = formatterYear.format(date);
      saveDateCurrent(yearDate, monthDate);
      _year = yearDate;
      _monthNumber = int.parse(monthDate);
      returnNameMonth(monthDate);
    });
    _animationController =
        AnimationController(vsync: this, duration: _animationDuration);
    isSideBarOpenedStreamController = PublishSubject<bool>();
    isSideBarOpenedStream = isSideBarOpenedStreamController.stream;
    isSideBarOpenedSink = isSideBarOpenedStreamController.sink;
  }

  void saveDateCurrent(String year, String month) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove("yearData");
    sharedPreferences.remove("monthData");
    sharedPreferences.setString("yearData", year);
    sharedPreferences.setString("monthData", month);
  }

  showAlert() {
    StatusAlert.show(
      context,
      blurPower: 10,
      titleOptions: StatusAlertTextConfiguration(
        style: TextStyle(
          fontFamily: 'Lato',
          fontWeight: FontWeight.bold,
          fontSize: 14,
          ),
      ),
      subtitleOptions: StatusAlertTextConfiguration(
        style: TextStyle(
          fontFamily: 'Lato',
          fontWeight: FontWeight.bold,
          fontSize: 12,
          ),
      ),
      duration: Duration(seconds: 2),
      title: 'Filtros de busqueda configurados',
      subtitle: " Mes: " + _month + " Año: " + _year,
      configuration: IconConfiguration(icon: AntDesign.check),
    );
  }

  void returnNameMonth(String numMonth) {
    switch (numMonth) {
      case "01":
        {
          setState(() {
            return _month = "Enero";
          });
        }
        break;
      case "02":
        {
          setState(() {
            return _month = "Febrero";
          });
        }
        break;
      case "03":
        {
          setState(() {
            return _month = "Marzo";
          });
        }
        break;
      case "04":
        {
          setState(() {
            return _month = "Abril";
          });
        }
        break;
      case "05":
        {
          setState(() {
            return _month = "Mayo";
          });
        }
        break;
      case "06":
        {
          setState(() {
            return _month = "Junio";
          });
        }
        break;
      case "07":
        {
          setState(() {
            return _month = "Julio";
          });
        }
        break;
      case "08":
        {
          setState(() {
            return _month = "Agosto";
          });
        }
        break;
      case "09":
        {
          setState(() {
            return _month = "Septiembre";
          });
        }
        break;
      case "10":
        {
          setState(() {
            return _month = "Octubre";
          });
        }
        break;
      case "11":
        {
          setState(() {
            return _month = "Noviembre";
          });
        }
        break;
      case "12":
        {
          setState(() {
            return _month = "Diciembre";
          });
        }
        break;
    }
  }

  @override
  void dispose() {
    _animationController.dispose();
    isSideBarOpenedStreamController.close();
    isSideBarOpenedSink.close();
    super.dispose();
  }

  void iconPress() {
    final animationStatus = _animationController.status;
    final isAnimationCompleted = animationStatus == AnimationStatus.completed;

    if (isAnimationCompleted) {
      isSideBarOpenedSink.add(false);
      _animationController.reverse();
    } else {
      isSideBarOpenedSink.add(true);
      _animationController.forward();
    }
  }

  @override
  Widget build(BuildContext context) {
    final screnWidth = MediaQuery.of(context).size.width;
    return StreamBuilder<bool>(
      initialData: false,
      stream: isSideBarOpenedStream,
      builder: (context, isSideBarOpenedAsync) {
        return AnimatedPositioned(
          duration: _animationDuration,
          top: 30,
          bottom: 0,
          left: isSideBarOpenedAsync.data ? 0 : -screnWidth,
          right: isSideBarOpenedAsync.data ? 0 : screnWidth - 35,
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  color: Color(0xFFE53935),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 10,
                      ),
                      Divider(
                        height: 45,
                        thickness: 0.7,
                        indent: 32,
                        endIndent: 32,
                        color: Colors.white.withOpacity(0.4),
                      ),

                      GestureDetector(
                        onTap: () {
                          showMonthPicker(
                            context: context,
                            initialDate:
                                DateTime(int.parse(_year), _monthNumber),
                            firstDate: DateTime(2018, 11),
                            lastDate: DateTime.now(),
                          ).then((date) {
                            var formatter = new DateFormat('MM');
                            var formatterYear = new DateFormat('yyyy');
                            String monthDate = formatter.format(date);
                            String yearDate = formatterYear.format(date);
                            returnNameMonth(monthDate);
                            setState(() {
                              _year = yearDate;
                              _monthNumber = int.parse(monthDate);
                              saveDateCurrent(yearDate, monthDate);
                              BlocProvider.of<NavigationBloc>(context)
                                  .add(NavigationEvents.HomePageClickedEvent);
                            });
                            showAlert();
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.only(left: 20),
                          child: Row(children: <Widget>[
                            Icon(
                              AntDesign.filter,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 25,
                            ),
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Text(
                                    "Mes : " + _month,
                                    style: TextStyle(
                                        fontFamily: 'Lato',
                                        color: Colors.white,
                                        fontSize: 14),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Text(
                                    "Año : " + _year,
                                    style: TextStyle(
                                        fontFamily: 'Lato',
                                        color: Colors.white,
                                        fontSize: 14),
                                  ),
                                  SizedBox(
                                    width: 30,
                                  ),
                                  Icon(
                                    Icons.keyboard_arrow_down,
                                    color: Colors.white60,
                                  )
                                ],
                              ),
                            )
                          ]),
                        ),
                      ),
                      Divider(
                          height: 45,
                          thickness: 0.7,
                          indent: 32,
                          endIndent: 32,
                          color: Colors.white.withOpacity(0.4)),
                      MenuItem(
                        icon: Icons.monetization_on,
                        title: 'Dashboard Ventas',
                        onTap: () {
                          iconPress();
                          BlocProvider.of<NavigationBloc>(context)
                              .add(NavigationEvents.HomePageClickedEvent);
                        },
                      ),
                      MenuItem(
                        icon: Icons.equalizer,
                        title: 'Venta al día',
                        onTap: () {
                          iconPress();
                          BlocProvider.of<NavigationBloc>(context)
                              .add(NavigationEvents.DailySaleGlobalPageEvent);
                        },
                      ),
                      Divider(
                        height: 45,
                        thickness: 0.7,
                        indent: 32,
                        endIndent: 32,
                        color: Colors.white.withOpacity(0.4),
                      ),
                      // MenuItem(
                      //   icon: Icons.help_outline,
                      //   title: 'Ayuda',
                      // ),
                      MenuItem(
                        icon: Icons.exit_to_app,
                        title: 'Salir',
                        onTap: () {
                          logOut(context);
                        },
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment(0, -0.9),
                child: GestureDetector(
                  onTap: () {
                    iconPress();
                  },
                  child: ClipPath(
                    clipper: CustomMenuClipper(),
                    child: Container(
                      alignment: Alignment.centerLeft,
                      color: Color(0xFFE53935),
                      width: 35,
                      height: 110,
                      child: AnimatedIcon(
                        progress: _animationController.view,
                        icon: AnimatedIcons.menu_close,
                        color: Colors.white,
                        size: 25,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}

class CustomMenuClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Paint paint = Paint();
    paint.color = Colors.white;

    final width = size.width;
    final height = size.height;

    Path path = Path();
    path.moveTo(0, 0);
    path.quadraticBezierTo(0, 8, 10, 16);
    path.quadraticBezierTo(width - 1, height / 2 - 20, width, height / 2);
    path.quadraticBezierTo(width + 1, height / 2 + 20, 10, height - 16);
    path.quadraticBezierTo(0, height - 8, 0, height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}
