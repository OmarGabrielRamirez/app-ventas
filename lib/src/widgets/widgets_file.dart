
import 'package:flutter/material.dart';

Widget homeScaffold(BuildContext context) {
  return Scaffold(
    body: Stack(children: <Widget>[
      _bannerHome(),
    ]),
    backgroundColor: Colors.blueGrey[900],
  );
}

Widget _bannerHome() {
  return ListView(
    children: <Widget>[
      Container(
        padding: EdgeInsets.only(top: 20, left: 30, right: 30),
        child: Container(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                  Text(
                  "Global",
                  style: TextStyle(
                      fontSize: 24,
                      fontFamily: 'Lato',
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                ),

                ],)
              ),
              Container(
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: Text(
                  '\$' + '220,000,000',
                  style: TextStyle(
                      fontSize: 35,
                      fontFamily: 'Lato Light',
                      fontWeight: FontWeight.w100,
                      color: Colors.white),
                ),
              ),
            ],
          ),
        ),
      ),
      indica(),
      Divider(
          height: 20,
          thickness: 0.5,
          indent: 20,
          endIndent: 20,
          color: Colors.white.withOpacity(0.4)),
    ],
  );
}

Widget indica() {
  return Container(
    child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
      Row(
        children: <Widget>[
          Text(
            "TBD",
            style: TextStyle(
                fontSize: 15,
                fontFamily: 'Lato Light',
                fontWeight: FontWeight.w300,
                color: Colors.white),
          ),
          Icon(Icons.arrow_drop_up, size: 35, color: Colors.green),
          Text(
            "10",
            style: TextStyle(
                fontSize: 15,
                fontFamily: 'Lato Light',
                fontWeight: FontWeight.w300,
                color: Colors.green[400]),
          )
        ],
      ),
      SizedBox(
        width: 10,
      ),
      Row(
        children: <Widget>[
          Text(
            "TLW",
            style: TextStyle(
                fontSize: 15,
                fontFamily: 'Lato Light',
                fontWeight: FontWeight.w300,
                color: Colors.white),
          ),
          Icon(Icons.arrow_drop_up, size: 35, color: Colors.green),
          Text(
            "4",
            style: TextStyle(
                fontSize: 15,
                fontFamily: 'Lato Light',
                fontWeight: FontWeight.w300,
                color: Colors.green[400]),
          )
        ],
      ),
      SizedBox(
        width: 10,
      ),
      Row(
        children: <Widget>[
          Text(
            "TLM",
            style: TextStyle(
                fontSize: 15,
                fontFamily: 'Lato Light',
                fontWeight: FontWeight.w300,
                color: Colors.white),
          ),
          Icon(Icons.arrow_drop_down, size: 35, color: Colors.red),
          Text(
            "4",
            style: TextStyle(
                fontSize: 15,
                fontFamily: 'Lato Light',
                fontWeight: FontWeight.w300,
                color: Colors.red),
          )
        ],
      ),
      SizedBox(
        width: 10,
      ),
      Row(
        children: <Widget>[
          Text(
            "TLY",
            style: TextStyle(
                fontSize: 15,
                fontFamily: 'Lato Light',
                fontWeight: FontWeight.w300,
                color: Colors.white),
          ),
          Icon(Icons.arrow_drop_up, size: 35, color: Colors.green),
          Text(
            "6",
            style: TextStyle(
                fontSize: 15,
                fontFamily: 'Lato Light',
                fontWeight: FontWeight.w300,
                color: Colors.green[400]),
          )
        ],
      ),
    ]),
  );
}

Widget footerLogin(String year) {
  return Container(
      padding: EdgeInsets.only(top: 270, left: 30, right: 30),
      alignment: Alignment.bottomCenter,
      child: Row(
        children: <Widget>[
          Text(
            "Eric Kayser México - Carmela & Sal",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w300,
              fontSize: 12,
              fontFamily: 'Lato',
            ),
          ),
          SizedBox(
            width: 70,
          ),
          Text(year,
              style: TextStyle(
                  fontFamily: 'Lato',
                  color: Colors.white,
                  fontWeight: FontWeight.w300,
                  fontSize: 12))
        ],
      ));
}

Widget loginLogo() {
  return Container(
    width: 250,
    margin: EdgeInsets.only(top: 10, bottom: 10, left: 50, right: 50),
    child: Center(
      child: Image.asset("assets/images/mk_logo.png"),
    ),
  );
}

Widget textFieldEmail(TextEditingController _emailController) {
  return Container(
    alignment: Alignment.bottomCenter,
    padding: EdgeInsets.only(left: 30, right: 30),
    child: TextField(
      style: TextStyle(
          fontFamily: 'Lato',
          fontWeight: FontWeight.w300,
          color: Colors.white,
          fontSize: 16),
      controller: _emailController,
      cursorColor: Color(0xFFB71C1C),
      decoration: InputDecoration(
        prefixIcon: Icon(
          Icons.email,
          size: 17.0,
          color: Colors.white,
        ),
        contentPadding: EdgeInsets.only(top: 20, bottom: 10),
        hintStyle: TextStyle(
            fontFamily: 'Lato',
            fontWeight: FontWeight.w300,
            fontSize: 16,
            color: Colors.white70),
        labelStyle: TextStyle(
            fontFamily: 'Lato',
            fontWeight: FontWeight.w300,
            fontSize: 16,
            color: Colors.white),
        labelText: "Email",
        hintText: "ejemplo@prigo.com.mx",
        enabledBorder: const UnderlineInputBorder(
          borderSide: const BorderSide(
            width: 0.7,
            color: Colors.white,
          ),
        ),
        border: const OutlineInputBorder(
          borderSide: const BorderSide(width: 1.0),
        ),
        focusedBorder: const UnderlineInputBorder(
          borderSide: const BorderSide(
            width: 1,
            color: Color(0XFFEF5350),
          ),
        ),
      ),
    ),
  );
}


class Sales{
  int monthval;
  int salesval;
}