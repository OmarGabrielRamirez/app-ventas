
import 'package:app_ventasmk/src/pages/details_globalnew.dart';
import 'package:app_ventasmk/src/pages/home_page.dart';
import 'package:app_ventasmk/src/sidebar/sidebar_layout.dart';
import 'package:app_ventasmk/src/pages/login_page.dart';
import 'package:flutter/material.dart';

Map<String, WidgetBuilder> getAppRoutes() {
  return  <String, WidgetBuilder>{
    '/': (BuildContext context) =>  SideBarLayout(),
    'home': (BuildContext context) => HomePage(),
    'login' : (BuildContext context) => LoginPage(),
    'global_details': (BuildContext context) => OneGlobalPage()
  };
}
