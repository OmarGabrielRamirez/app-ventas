import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:json_table/json_table.dart';
import 'package:json_table/json_table_column.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class DetailsMontlySuc extends StatefulWidget {
  final String idSuc;
  DetailsMontlySuc(this.idSuc);

  @override
  DetailsMontlySucState createState() => DetailsMontlySucState();
}

class DetailsMontlySucState extends State<DetailsMontlySuc> {
  List<JsonTableColumn> columns;
  var _json;
  String _idSuc;
  fetchDataTable() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var month = sharedPreferences.getString("monthData");
    var year = sharedPreferences.getString("yearData");
    Map dataPost = {
      'id': _idSuc,
      'monthData': month.toString(),
      'yearData': year.toString()
    };
    var data = await http.post(
        "https://intranet.prigo.com.mx/api/getglobalmontlysuc",
        body: dataPost);
    var json = jsonDecode(data.body);
    setState(() {
      _json = json;
    });
    return json;
  }

  Future<SubsidiarySale> getInfoSales() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var month = sharedPreferences.getString("monthData");
    var year = sharedPreferences.getString("yearData");
    Map data = {
      'idSuc': _idSuc,
      'monthData': month.toString(),
      'yearData': year.toString()
    };
    try {
      var response = await http.post(
          "https://intranet.prigo.com.mx/api/getglobalmontlydetailssuc",
          body: data);
      if (response.statusCode == 200) {
        var dataSalesMontly = json.decode(response.body);

        SubsidiarySale sub = SubsidiarySale(
            dataSalesMontly['idSubsidiary'].toString(),
            dataSalesMontly['nameSubsidiary'],
            dataSalesMontly['saleCurrentDaily'],
            dataSalesMontly['percentageTBD'].toStringAsFixed(2),
            dataSalesMontly['percentageTLY'].toStringAsFixed(2),
            dataSalesMontly['percentageTLM'].toStringAsFixed(2));
        return sub;
      } else {
        SubsidiarySale sm = SubsidiarySale(
            "no_data", "no_data", "no_data", "no_data", "no_data", "no_data");
        return sm;
      }
    } catch (e) {
      SubsidiarySale sm = SubsidiarySale(
          "connection_network_failed",
          "connection_network_failed",
          "connection_network_failed",
          "connection_network_failed",
          "connection_network_failed",
          "connection_network_failed");
      return sm;
    }
  }

  Widget setIcon(total) {
    total = double.parse(total);
    if (total > 0)
      return Icon(Icons.arrow_drop_up, size: 27, color: Colors.green[900]);
    else
      return Icon(Icons.arrow_drop_down, size: 27, color: Colors.red[700]);
  }

  Widget setTextIndicator(total) {
    total = double.parse(total);
    if (total > 0)
      return Text(
        total.toString(),
        style: TextStyle(
            fontSize: 11,
            fontFamily: 'Lato Light',
            fontWeight: FontWeight.bold,
            color: Colors.green[900]),
      );
    else
      return Text(
        total.toString(),
        style: TextStyle(
            fontSize: 12,
            fontFamily: 'Lato Light',
            fontWeight: FontWeight.bold,
            color: Colors.red[700]),
      );
  }

  Widget indicator(total, text) {
    return Container(
      padding: EdgeInsets.all(0.0),
      child: Row(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(0.0),
            child: Text(
              text,
              style: TextStyle(
                  fontSize: 11,
                  fontFamily: 'Lato Light',
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
          ),
          setIcon(total),
          setTextIndicator(total)
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      _idSuc = widget.idSuc;
    });
    this.fetchDataTable();
    columns = [
      JsonTableColumn("fecha", label: "Fecha"),
      JsonTableColumn("ventaTotal", label: "Venta total")
    ];
  }

  Widget jsonTable(_json) {
    if (_json != null) {
      return JsonTable(
        _json,
        columns: columns,
        tableHeaderBuilder: (String header) {
          return Container(
            width: MediaQuery.of(context).size.width / 2 - 23,
            padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(3),
                color: Colors.grey.withOpacity(0.5),
                border:
                    Border.all(width: 1, color: Colors.grey.withOpacity(0.5))),
            child: Text(
              header,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.display1.copyWith(
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Lato',
                  fontSize: 14.0,
                  color: Colors.black),
            ),
          );
        },
        tableCellBuilder: (value) {
          return Container(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.2),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2),
                border:
                    Border.all(width: 1, color: Colors.grey.withOpacity(0.5))),
            child: Text(
              value,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.display1.copyWith(
                  fontWeight: FontWeight.w600,
                  fontFamily: 'Lato',
                  fontSize: 13.0,
                  color: Colors.black87),
            ),
          );
        },
        showColumnToggle: false,
        allowRowHighlight: true,
        rowHighlightColor: Colors.yellow[500].withOpacity(0.9),
        paginationRowCount: 50,
      );
    } else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SpinKitCircle(
            color: Colors.black,
            size: 40.0,
          ),
          JumpingText(
            'Cargando...',
            style: TextStyle(
                fontFamily: 'Lato', fontWeight: FontWeight.bold, fontSize: 12),
          ),
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey[900],
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.pop(context);
          }),
      body: Container(
        child: Center(
          child: FutureBuilder(
              future: getInfoSales(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.data == null) {
                  return Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(
                              10.0) //                 <--- border radius here
                          ),
                    ),
                    height: double.infinity,
                    width: double.infinity,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SpinKitCircle(
                          color: Colors.white,
                          size: 40.0,
                        ),
                        JumpingText(
                          'Cargando...',
                          style: TextStyle(
                              fontFamily: 'Lato',
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 12),
                        ),
                      ],
                    ),
                  );
                } else if (snapshot.data.saleCurrent == 'no_data') {
                  return Container(
                    child: Center(
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                          Icon(
                            Icons.error,
                            size: 90,
                            color: Colors.white38,
                          ),
                          Text(
                            "INFORMACIÓN NO DISPONIBLE",
                            style: TextStyle(
                                fontSize: 12,
                                fontFamily: 'Lato',
                                fontWeight: FontWeight.w100,
                                color: Colors.white),
                          ),
                          Text(
                            "¿Problemas?",
                            style: TextStyle(
                                fontSize: 11,
                                fontFamily: 'Lato',
                                fontWeight: FontWeight.w100,
                                color: Colors.white),
                          ),
                          Text(
                            "sit@prigo.com.mx",
                            style: TextStyle(
                                fontSize: 11,
                                fontFamily: 'Lato',
                                fontWeight: FontWeight.w100,
                                color: Colors.white),
                          )
                        ])),
                  );
                } else if (snapshot.data.saleCurrent ==
                    'connection_network_failed') {
                  return Container(
                    child: Center(
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                          Icon(
                            Icons.error,
                            size: 90,
                            color: Colors.white38,
                          ),
                          Text(
                            "SIN CONEXIÓN A INTERNET",
                            style: TextStyle(
                                fontSize: 12,
                                fontFamily: 'Lato',
                                fontWeight: FontWeight.w100,
                                color: Colors.white),
                          ),
                          Text(
                            "Habilita tus datos móviles o red Wi-Fi ",
                            style: TextStyle(
                                fontSize: 11,
                                fontFamily: 'Lato',
                                fontWeight: FontWeight.w100,
                                color: Colors.white),
                          ),
                        ])),
                  );
                } else {
                  return Container(
                    margin: EdgeInsets.only(
                        top: 45, left: 20, right: 20, bottom: 45),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width - 40,
                            child: Card(
                              elevation: 10,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(top: 10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          "Venta mensual",
                                          style: TextStyle(
                                              fontSize: 9,
                                              fontFamily: 'Lato',
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          snapshot.data.name,
                                          style: TextStyle(
                                              fontSize: 20,
                                              fontFamily: 'Lato',
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(top: 5),
                                    child: Text(
                                      '\$' + snapshot.data.saleCurrent,
                                      style: TextStyle(
                                          fontSize: 25,
                                          fontFamily: 'Lato Light',
                                          fontWeight: FontWeight.w300,
                                          color: Colors.black),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(bottom: 5),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        indicator(
                                            snapshot.data.percentajeTBD, 'TBD'),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        indicator(
                                            snapshot.data.percentajeTLY, 'TLY'),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        indicator(
                                            snapshot.data.percentajeTLM, 'TLM'),
                                        SizedBox(
                                          width: 5,
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            child: Card(
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(3)),
                                width: MediaQuery.of(context).size.width - 40,
                                child: jsonTable(_json),
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                            ),
                          )
                        ]),
                  );
                }
              }),
        ),
      ),
    );
  }
}

class SubsidiarySale {
  final String id;
  final String name;
  final String saleCurrent;
  final String percentajeTBD;
  final String percentajeTLY;
  final String percentajeTLM;

  SubsidiarySale(this.id, this.name, this.saleCurrent, this.percentajeTBD,
      this.percentajeTLY, this.percentajeTLM);
}
