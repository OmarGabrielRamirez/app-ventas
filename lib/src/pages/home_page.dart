import 'package:app_ventasmk/src/pages/details_newsuc.dart';
import 'package:flutter/services.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:app_ventasmk/src/bloc.navigation_menu/bloc_navigation.dart';
import 'dart:io';
import 'details_globalnew.dart';

class HomePage extends StatefulWidget with NavigationStates {
  final seriesList = List<charts.Series<TabelGrafik, DateTime>>();
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with NavigationStates {
  final seriesList = List<charts.Series<TabelGrafik, DateTime>>();
  List<TabelGrafik> datagrafik = new List();
  Map<String, dynamic> map;
  bool isShowingMainData;

  Future<List<charts.Series<TabelGrafik, DateTime>>> getDataCharts() async {
  

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var accessToken = sharedPreferences.getString("access_token");
    var month = sharedPreferences.getString("monthData");
    var year = sharedPreferences.getString("yearData");
    var id = sharedPreferences.getInt("id_user");
    Map data = {
      'idUser': id.toString(),
      'access_token': accessToken,
      'monthData': month.toString(),
      'yearData': year.toString()
    };
    var response = await http.post(
        "https://intranet.prigo.com.mx/api/sales/getglobalpreviusmonths",
        body: data);
    if (response.statusCode == 200) {
      final dataResponse = jsonDecode(response.body);
    
      datagrafik.clear();

      dataResponse.forEach((api) {
        datagrafik.add(
            TabelGrafik(DateTime.parse(api['date']), (api['total_sales'])));
      });
      sleep(const Duration(milliseconds:100));
    }
    seriesList.clear();
    
    seriesList.add(
      charts.Series(
        colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
        areaColorFn: (_, __) => charts.MaterialPalette.red.shadeDefault.lighter,
        id: 'Sales',
        domainFn: (TabelGrafik sales, _) => sales.tanggal,
        measureFn: (TabelGrafik sales, _) => sales.dinar,
        data: datagrafik,
      ),
    );
    
    return seriesList;
  }

  notRotate() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  checkLoginStatus() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("access_token") == null) {
      Navigator.pushNamed(context, 'login');
    }
  }

  void initState() {
    super.initState();
    isShowingMainData = true;
    checkLoginStatus();
  }

  Future<GlobalMontlySale> getInfoSales() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var idUser = sharedPreferences.getInt("id_user");
    var accessToken = sharedPreferences.getString("access_token");
    var month = sharedPreferences.getString("monthData");
    var year = sharedPreferences.getString("yearData");
    Map data = {
      'idUser': idUser.toString(),
      'accessToken': accessToken,
      'monthData': month,
      'yearData': year
    };
    try {
      var response = await http.post(
          "https://intranet.prigo.com.mx/api/sales/getglobalmonthsale",
          body: data);
      if (response.statusCode == 200) {
       
        var dataSalesMontly = json.decode(response.body);

        GlobalMontlySale sm = GlobalMontlySale(
            dataSalesMontly["reportMonth"],
            dataSalesMontly["salesMonthlyCurrent"],
            dataSalesMontly["percentageTBD"],
            dataSalesMontly["percentageTLY"],
            dataSalesMontly["percentageTLM"],
            dataSalesMontly["percentageTLW"]);

        return sm;
      } else {
        GlobalMontlySale sm = GlobalMontlySale(
            "no_data", "no_data", "no_data", "no_data", "no_data", "no_data");
        return sm;
      }
    } catch (e) {
      GlobalMontlySale sm = GlobalMontlySale(
          "connection_network_failed",
          "connection_network_failed",
          "connection_network_failed",
          "connection_network_failed",
          "connection_network_failed",
          "connection_network_failed");
      return sm;
    }
  }

  Future<List<SubsidiarySale>> getDataSub() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var accessToken = sharedPreferences.getString("access_token");
    var id = sharedPreferences.getInt("id_user");
    var month = sharedPreferences.getString("monthData");
    var year = sharedPreferences.getString("yearData");
    Map data = {
      'idUser': id.toString(),
      'accessToken': accessToken,
      'monthData': month.toString(),
      'yearData': year.toString()
    };

    var response = await http
        .post("https://intranet.prigo.com.mx/api/getsubsidiaries", body: data);
    var dataSub = json.decode(response.body);
    List<SubsidiarySale> subs = [];
    for (var x in dataSub) {
      SubsidiarySale sub = SubsidiarySale(
          x['idSubsidiary'].toString(),
          x['nameSubsidiary'],
          x['saleCurrentDaily'],
          x['percentageTBD'].toStringAsFixed(2),
          x['percentageTLY'].toStringAsFixed(2),
          x['percentageTLM'].toStringAsFixed(2));
      subs.add(sub);
    }

    return subs;
  }

  Widget setIcon(total) {
    total = double.parse(total);
    if (total > 0)
      return Container(
          child: Icon(
        Icons.arrow_drop_up,
        size: 27,
        color: Colors.green[900],
      ));
    else
      return Container(
          child: Icon(Icons.arrow_drop_down, size: 27, color: Colors.red[700]));
  }

  Widget setTextIndicator(total) {
    total = double.parse(total);
    if (total > 0)
      return Text(
        total.toString(),
        style: TextStyle(
            fontSize: 12,
            fontFamily: 'Lato Light',
            fontWeight: FontWeight.bold,
            color: Colors.green[900]),
      );
    else
      return Text(
        total.toString(),
        style: TextStyle(
            fontSize: 12,
            fontFamily: 'Lato Light',
            fontWeight: FontWeight.bold,
            color: Colors.red[700]),
      );
  }

  Widget indicator(total, text) {
    return Container(
      padding: EdgeInsets.all(0.0),
      child: Row(
        children: <Widget>[
          Text(
            text,
            style: TextStyle(
                fontSize: 12,
                fontFamily: 'Lato Light',
                fontWeight: FontWeight.bold,
                color: Colors.black),
          ),
          setIcon(total),
          setTextIndicator(total)
        ],
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    notRotate();
    return Scaffold(
      backgroundColor: Colors.blueGrey[900],
      body: Container(
        child: FutureBuilder(
          future: getInfoSales(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(
                          10.0) //                 <--- border radius here
                      ),
                ),
                height: double.infinity,
                width: double.infinity,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SpinKitCircle(
                      color: Colors.white,
                      size: 40.0,
                    ),
                    JumpingText(
                      'Cargando...',
                      style: TextStyle(
                          fontFamily: 'Lato',
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 12),
                    ),
                  ],
                ),
              );
            } else if (snapshot.data.totalSales == 'no_data') {
              return Container(
                child: Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                      Icon(
                        Icons.error,
                        size: 90,
                        color: Colors.white38,
                      ),
                      Text(
                        "INFORMACIÓN NO DISPONIBLE",
                        style: TextStyle(
                            fontSize: 12,
                            fontFamily: 'Lato',
                            fontWeight: FontWeight.w100,
                            color: Colors.white),
                      ),
                      Text(
                        "¿Problemas?",
                        style: TextStyle(
                            fontSize: 11,
                            fontFamily: 'Lato',
                            fontWeight: FontWeight.w100,
                            color: Colors.white),
                      ),
                      Text(
                        "sit@prigo.com.mx",
                        style: TextStyle(
                            fontSize: 11,
                            fontFamily: 'Lato',
                            fontWeight: FontWeight.w100,
                            color: Colors.white),
                      )
                    ])),
              );
            } else if (snapshot.data.totalSales ==
                'connection_network_failed') {
              return Container(
                child: Center(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          size: 90,
                          color: Colors.white38,
                        ),
                        Text(
                          "INFORMACIÓN NO DISPONIBLE",
                          style: TextStyle(
                              fontSize: 12,
                              fontFamily: 'Lato',
                              fontWeight: FontWeight.w100,
                              color: Colors.white),
                        ),
                        Text(
                          "¿Problemas?",
                          style: TextStyle(
                              fontSize: 11,
                              fontFamily: 'Lato',
                              fontWeight: FontWeight.w100,
                              color: Colors.white),
                        ),
                        Text(
                          "sit@prigo.com.mx",
                          style: TextStyle(
                              fontSize: 11,
                              fontFamily: 'Lato',
                              fontWeight: FontWeight.w100,
                              color: Colors.white),
                        )
                      ]),
                ),
              );
            } else {
              return Container(
                margin:
                    EdgeInsets.only(top: 45, left: 20, right: 20, bottom: 45),
                child: ListView(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width - 40,
                      child: Stack(
                        fit: StackFit.passthrough,
                        children: <Widget>[
                          Column(children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => OneGlobalPage()));
                              },
                              child: Container(
                                child: Card(
                                  elevation: 10,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(top: 10),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text(
                                              "Venta mensual",
                                              style: TextStyle(
                                                  fontSize: 9,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.black),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text(
                                              "Global",
                                              style: TextStyle(
                                                  fontSize: 20,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.black),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(top: 5),
                                        child: Text(
                                          '\$' + snapshot.data.totalSales,
                                          style: TextStyle(
                                              fontSize: 25,
                                              fontFamily: 'Lato Light',
                                              fontWeight: FontWeight.w300,
                                              color: Colors.black),
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(bottom: 5),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            indicator(
                                                snapshot.data.percentajeTBD,
                                                'TBD'),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            indicator(
                                                snapshot.data.percentajeTLY,
                                                'TLY'),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            indicator(
                                                snapshot.data.percentajeTLM,
                                                'TLM'),
                                            SizedBox(
                                              width: 5,
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                                child: Container(
                              padding: EdgeInsets.all(0),
                              child: FutureBuilder(
                                future: getDataSub(),
                                builder: (BuildContext context,
                                    AsyncSnapshot snapshot) {
                                  if (snapshot.data == null) {
                                    return Container(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          SpinKitCircle(
                                            color: Colors.white,
                                            size: 40.0,
                                          ),
                                          JumpingText(
                                            'Cargando...',
                                            style: TextStyle(
                                                fontFamily: 'Lato',
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 12),
                                          ),
                                        ],
                                      ),
                                    );
                                  } else {
                                   return AnimationLimiter(
                                     child: ListView.builder(
                                      itemCount: snapshot.data.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                              return AnimationConfiguration.staggeredList(
                                                position: index, 
                                                duration: const Duration(milliseconds: 675),
                                                child: SlideAnimation(
                                                   verticalOffset: 70.0,
                                                  child:FlipAnimation(
                                                    child:   Container(
                                          child: GestureDetector(
                                            onTap: () {
                                              Navigator.of(context).push(
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          DetailsMontlySucNew(
                                                              snapshot
                                                                  .data[index]
                                                                  .id
                                                                  .toString())));
                                            },
                                            child: Card(
                                              elevation: 10,
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(15.0),
                                              ),
                                              child: Column(
                                                children: <Widget>[
                                                  Container(
                                                    padding: EdgeInsets.only(
                                                        top: 10, bottom: 5),
                                                    child: Text(
                                                      snapshot.data[index].name,
                                                      style: TextStyle(
                                                          fontSize: 20,
                                                          fontFamily: 'Lato',
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: Colors.black),
                                                    ),
                                                  ),
                                                  Container(
                                                    padding:
                                                        EdgeInsets.only(top: 1),
                                                    child: Text(
                                                      '\$' +
                                                          snapshot.data[index]
                                                              .saleCurrent,
                                                      style: TextStyle(
                                                          fontSize: 25,
                                                          fontFamily:
                                                              'Lato Light',
                                                          fontWeight:
                                                              FontWeight.w300,
                                                          color: Colors.black),
                                                    ),
                                                  ),
                                                  Container(
                                                    padding: EdgeInsets.only(
                                                        bottom: 5),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: <Widget>[
                                                        indicator(
                                                            snapshot.data[index]
                                                                .percentajeTBD,
                                                            'TBD'),
                                                        SizedBox(
                                                          width: 5,
                                                        ),
                                                        indicator(
                                                            snapshot.data[index]
                                                                .percentajeTLY,
                                                            'TLY'),
                                                        SizedBox(
                                                          width: 5,
                                                        ),
                                                        indicator(
                                                            snapshot.data[index]
                                                                .percentajeTLM,
                                                            'TLM'),
                                                        SizedBox(
                                                          width: 5,
                                                        ),
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                                  ),
                                                )
                                          );
                                      },
                                    ),

                                   );
                                  }
                                },
                              ),
                            ))
                          ])
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width - 40,
                      child: SingleChildScrollView(
                        child: ConstrainedBox(
                          constraints: BoxConstraints(maxHeight: 445),
                          child: Container(
                            width: MediaQuery.of(context).size.width - 40,
                            child: Card(
                              elevation: 20,
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                              child: Container(
                                child: FutureBuilder(
                                    future: getDataCharts(),
                                    builder: (BuildContext context,
                                        AsyncSnapshot snapshot) {
                                      if (snapshot.data == null) {
                                        return Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(
                                                    10.0) //                 <--- border radius here
                                                ),
                                          ),
                                          height: double.infinity,
                                          width: double.infinity,
                                          child: Center(
                                              child: SpinKitCircle(
                                            color: Colors.white,
                                            size: 30.0,
                                          )),
                                        );
                                      } else {
                                        return Flex(
                                          direction: Axis.vertical,
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.only(top: 10),
                                              child: Text(
                                                'Venta Mensual',
                                                style: TextStyle(
                                                    fontSize: 9,
                                                    fontFamily: 'Lato',
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.black),
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(top: 5),
                                              child: Text(
                                                'Global',
                                                style: TextStyle(
                                                    fontFamily: 'Lato',
                                                    color: Colors.black,
                                                    fontSize: 20.0,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                            Expanded(
                                              child: charts.TimeSeriesChart(
                                                  seriesList,
                                                  defaultRenderer:
                                                      charts.LineRendererConfig(
                                                          includePoints: true,
                                                          includeArea: true,
                                                          stacked: true),
                                                  animate: false,
                                                  dateTimeFactory: const charts
                                                      .LocalDateTimeFactory(),
                                                  behaviors: [
                                                    charts.ChartTitle('Mes',
                                                        behaviorPosition: charts
                                                            .BehaviorPosition
                                                            .bottom,
                                                        titleOutsideJustification:
                                                            charts
                                                                .OutsideJustification
                                                                .middleDrawArea),
                                                    charts.ChartTitle(
                                                        'Total (\$)',
                                                        behaviorPosition: charts
                                                            .BehaviorPosition
                                                            .start,
                                                        titleOutsideJustification:
                                                            charts
                                                                .OutsideJustification
                                                                .middleDrawArea),
                                                  ]),
                                            ),
                                          ],
                                        );
                                      }
                                    }),
                              ),
                              
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              );
            }
          },
        ),
      ),
    );
  }
}

class GlobalMontlySale {
  final String dateReport;
  final String totalSales;
  final String percentajeTBD;
  final String percentajeTLY;
  final String percentajeTLM;
  final String percentajeTLW;

  GlobalMontlySale(this.dateReport, this.totalSales, this.percentajeTBD,
      this.percentajeTLY, this.percentajeTLM, this.percentajeTLW);
}

class SubsidiarySale {
  final String id;
  final String name;
  final String saleCurrent;
  final String percentajeTBD;
  final String percentajeTLY;
  final String percentajeTLM;

  SubsidiarySale(this.id, this.name, this.saleCurrent, this.percentajeTBD,
      this.percentajeTLY, this.percentajeTLM);
}

class PreviusMontlySales {
  final String dateMontly;
  final double totalSales;
  final charts.Color barColor;

  PreviusMontlySales(this.dateMontly, this.totalSales, this.barColor);
}

class TabelGrafik {
  final DateTime tanggal;
  final double dinar;
  TabelGrafik(this.tanggal, this.dinar);
}
