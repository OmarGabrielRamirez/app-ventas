

import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:app_ventasmk/src/bloc.navigation_menu/bloc_navigation.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:json_table/json_table_column.dart';
import 'package:json_table/json_table.dart';
import 'package:http/http.dart' as http;

class DiarySaleGlobal extends StatefulWidget with NavigationStates {
  DiarySaleGlobal({Key key}) : super(key: key);

  @override
  DiarySaleGlobalState createState() => DiarySaleGlobalState();
}

class DiarySaleGlobalState extends State<DiarySaleGlobal>
    with NavigationStates {
  List<JsonTableColumn> columns;
  var _json;
  @override
  void initState() {
    columns = [
      JsonTableColumn("Sucursal", label: "Sucursal"),
      JsonTableColumn("Venta al día", label: "Al día"),
      JsonTableColumn("Budget al día", label: "Budget al día"),
      JsonTableColumn("% TBD", label: "%"),
      JsonTableColumn("Año Anterior", label: "Año Anterior"),
      JsonTableColumn("% TLY", label: "%"),
      JsonTableColumn("Proyectada", label: "Proyectada"),
      JsonTableColumn("Budget al mes", label: "Budget al mes"),
      JsonTableColumn("% TBM", label: "%"),
      JsonTableColumn("Cheques", label: "Cheques"),
      JsonTableColumn("% Cheques", label: "%"),
      JsonTableColumn("Cheque promedio", label: "Cheque Promedio"),
      JsonTableColumn("% Cheque promedio", label: "%"),
    ];
    this.getInfoReportSalesDaily();
    super.initState();
  }

  getInfoReportSalesDaily() async {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      var idUser = sharedPreferences.getInt("id_user");
  var month = sharedPreferences.getString("monthData");
  var year = sharedPreferences.getString("yearData");
  Map dataB = {
    'idUser': idUser.toString(),
    'monthData': month.toString(),
    'yearData': year.toString()
  };
    var data = await http
        .post("https://intranet.prigo.com.mx/api/sales/getglobaldiarysale",body: dataB);
    var json = jsonDecode(data.body);
    setState(() {
      _json = json;
    });
  }

  Widget jsonTable(_json) {
    if (_json != null) {
      return JsonTable(
        _json,
        columns: columns,
        tableHeaderBuilder: (String header) {
          return Container(
            padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2),
                color: Colors.grey.withOpacity(0.5),
                border:
                    Border.all(width: 1, color: Colors.grey.withOpacity(0.5))),
            child: Text(
              header,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.display1.copyWith(
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Lato',
                  fontSize: 14.0,
                  color: Colors.black),
            ),
          );
        },
        tableCellBuilder: (value) {
          return Container(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.2),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2),
                border:
                    Border.all(width: 1, color: Colors.grey.withOpacity(0.5))),
            child: Text(
              value,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.display1.copyWith(
                  fontWeight: FontWeight.w600,
                  fontFamily: 'Lato',
                  fontSize: 13.0,
                  color: Colors.black87),
            ),
          );
        },
        showColumnToggle: true,
        allowRowHighlight: true,
        rowHighlightColor: Colors.yellow[500].withOpacity(0.9),
        paginationRowCount: 50,
      );
    } else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SpinKitCircle(
            color: Colors.black,
            size: 40.0,
          ),
          JumpingText(
            'Cargando...',
            style: TextStyle(fontFamily: 'Lato', fontWeight: FontWeight.bold, fontSize: 12),
          ),
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey[900],
      body: Container(
        padding: EdgeInsets.only(top: 45, left: 20, right: 20, bottom: 45),
        child: Card(
          elevation: 10,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Container(
            width: MediaQuery.of(context).size.width - 40,
            child: Column(children: <Widget>[
              Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(
                          width: 3, color: Colors.grey.withOpacity(0.5))),
                  // color: Colors.red,
                  padding: EdgeInsets.only(top: 10, bottom: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.equalizer,
                        size: 16,
                        color: Colors.black,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        "Venta al día",
                        style: TextStyle(
                            fontFamily: 'Lato',
                            color: Colors.black,
                            fontWeight: FontWeight.w600,
                            fontSize: 16),
                      ),
                    ],
                  )),
              Expanded(child: jsonTable(_json))
            ]),
          ),
        ),
      ),
    );
  }
}

class GlobalDiarySale {
  final String totalSales;
  final String percentajeTBD;
  final String percentajeTLY;
  final String percentajeTLM;

  GlobalDiarySale(this.totalSales, this.percentajeTBD, this.percentajeTLY,
      this.percentajeTLM);
}
