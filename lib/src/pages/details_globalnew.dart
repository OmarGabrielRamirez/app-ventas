import 'package:app_ventasmk/src/pages/details_global.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class OneGlobalPage extends StatefulWidget {
  OneGlobalPage({Key key}) : super(key: key);

  @override
  OneGlobalPageState createState() => OneGlobalPageState();
}

Future<DetailsMonthSale> getInfoSalesDetails() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var idUser = sharedPreferences.getInt("id_user");
  var accessToken = sharedPreferences.getString("access_token");
  var month = sharedPreferences.getString("monthData");
  var year = sharedPreferences.getString("yearData");
  Map data = {
    'idUser' : idUser.toString(),
    'access_token': accessToken,
    'monthData': month.toString(),
    'yearData': year.toString()
  };
  try {
    var response = await http.post(
        "https://intranet.prigo.com.mx/api/getinfodetailsglobal",
        body: data);
    if (response.statusCode == 200) {
      var dataSalesMontlyDetails = json.decode(response.body);

      DetailsMonthSale sm = DetailsMonthSale(
          dataSalesMontlyDetails["forecast"],
          dataSalesMontlyDetails["promChecks"],
          dataSalesMontlyDetails["totalGuests"].toString(),
          dataSalesMontlyDetails["totalVit"],
          dataSalesMontlyDetails["totalSal"],
          dataSalesMontlyDetails["totalUber"],
          dataSalesMontlyDetails["totalOtros"],
          dataSalesMontlyDetails["totalTenderCreditCard"],
          dataSalesMontlyDetails["totalTenderDebitCard"],
          dataSalesMontlyDetails["totalTenderCashMXN"],
          dataSalesMontlyDetails["totalTenderCashUSD"],
          dataSalesMontlyDetails["totalTenderCashEURO"],
          dataSalesMontlyDetails["totalTenderUber"],
          dataSalesMontlyDetails["totalTenderOther"]);

      return sm;
    } else {
      return null;
    }
  } catch (e) {
    return null;
  }
}

Future<GlobalMontlySale> getInfoSales() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var idUser = sharedPreferences.getInt("id_user");
  var accessToken = sharedPreferences.getString("access_token");
  var month = sharedPreferences.getString("monthData");
  var year = sharedPreferences.getString("yearData");
  Map data = {
    'idUser': idUser.toString(),
    'accessToken': accessToken,
    'monthData': month,
    'yearData': year
  };
  try {
    var response = await http.post(
        "https://intranet.prigo.com.mx/api/sales/getglobalmonthsale",
        body: data);
    if (response.statusCode == 200) {
      var dataSalesMontly = json.decode(response.body);

      GlobalMontlySale sm = GlobalMontlySale(
          dataSalesMontly["reportMonth"],
          dataSalesMontly["salesMonthlyCurrent"],
          dataSalesMontly["percentageTBD"],
          dataSalesMontly["percentageTLY"],
          dataSalesMontly["percentageTLM"],
          dataSalesMontly["percentageTLW"]);

      return sm;
    } else {
      GlobalMontlySale sm = GlobalMontlySale(
          "no_data", "no_data", "no_data", "no_data", "no_data", "no_data");
      return sm;
    }
  } catch (e) {
    GlobalMontlySale sm = GlobalMontlySale(
        "connection_network_failed",
        "connection_network_failed",
        "connection_network_failed",
        "connection_network_failed",
        "connection_network_failed",
        "connection_network_failed");
    return sm;
  }
}

Widget setIcon(total) {
  total = double.parse(total);
  if (total > 0)
    return Icon(Icons.arrow_drop_up, size: 27, color: Colors.green[900]);
  else
    return Icon(Icons.arrow_drop_down, size: 27, color: Colors.red[700]);
}

Widget setTextIndicator(total) {
  total = double.parse(total);
  if (total > 0)
    return Text(
      total.toString(),
      style: TextStyle(
          fontSize: 11,
          fontFamily: 'Lato Light',
          fontWeight: FontWeight.bold,
          color: Colors.green[900]),
    );
  else
    return Text(
      total.toString(),
      style: TextStyle(
          fontSize: 12,
          fontFamily: 'Lato Light',
          fontWeight: FontWeight.bold,
          color: Colors.red[700]),
    );
}

Widget indicator(total, text) {
  return Container(
    padding: EdgeInsets.all(0.0),
    child: Row(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(0.0),
          child: Text(
            text,
            style: TextStyle(
                fontSize: 11,
                fontFamily: 'Lato Light',
                fontWeight: FontWeight.bold,
                color: Colors.black),
          ),
        ),
        setIcon(total),
        setTextIndicator(total)
      ],
    ),
  );
}

class OneGlobalPageState extends State<OneGlobalPage>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 200),
      lowerBound: 0.0,
      upperBound: 0.1,
    )..addListener(() {
        setState(() {});
      });
  }

  @override
  Widget build(BuildContext context) {
      var width = MediaQuery.of(context).size.width;
      var widthCard = (width-40)/3;
    return Scaffold(
      backgroundColor: Colors.blueGrey[900],
      body:Container(
            child: FutureBuilder(
                future: getInfoSales(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.data == null) {
                    return Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(
                                  10.0) //                 <--- border radius here
                              ),
                        ),
                        height: double.infinity,
                        width: double.infinity,
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SpinKitCircle(
                                color: Colors.white,
                                size: 40.0,
                              ),
                              JumpingText(
                                'Cargando...',
                                style: TextStyle(
                                    fontFamily: 'Lato',
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12),
                              ),
                            ],
                          ),
                        ));
                  } else if (snapshot.data.totalSales == 'no_data') {
                    return Container(
                      child: Center(
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                            Icon(
                              Icons.error,
                              size: 90,
                              color: Colors.white38,
                            ),
                            Text(
                              "INFORMACIÓN NO DISPONIBLE",
                              style: TextStyle(
                                  fontSize: 12,
                                  fontFamily: 'Lato',
                                  fontWeight: FontWeight.w100,
                                  color: Colors.white),
                            ),
                            Text(
                              "¿Problemas?",
                              style: TextStyle(
                                  fontSize: 11,
                                  fontFamily: 'Lato',
                                  fontWeight: FontWeight.w100,
                                  color: Colors.white),
                            ),
                            Text(
                              "sit@prigo.com.mx",
                              style: TextStyle(
                                  fontSize: 11,
                                  fontFamily: 'Lato',
                                  fontWeight: FontWeight.w100,
                                  color: Colors.white),
                            )
                          ])),
                    );
                  } else if (snapshot.data.totalSales ==
                      'connection_network_failed') {
                    return Container(
                      child: Center(
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                            Icon(
                              Icons.error,
                              size: 90,
                              color: Colors.white38,
                            ),
                            Text(
                              "SIN CONEXIÓN A INTERNET",
                              style: TextStyle(
                                  fontSize: 12,
                                  fontFamily: 'Lato',
                                  fontWeight: FontWeight.w100,
                                  color: Colors.white),
                            ),
                            Text(
                              "Habilita tus datos móviles o red Wi-Fi ",
                              style: TextStyle(
                                  fontSize: 11,
                                  fontFamily: 'Lato',
                                  fontWeight: FontWeight.w100,
                                  color: Colors.white),
                            ),
                          ])),
                    );
                  } else {
                    return Container(
                      margin: EdgeInsets.only(top: 45, left: 20, right: 20),
                      child: Column(
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width - 40,
                              child: Card(
                                elevation: 10,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.only(top: 10),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            "Venta mensual",
                                            style: TextStyle(
                                                fontSize: 9,
                                                fontFamily: 'Lato',
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            "Global",
                                            style: TextStyle(
                                                fontSize: 20,
                                                fontFamily: 'Lato',
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(top: 5),
                                      child: Text(
                                        '\$' + snapshot.data.totalSales,
                                        style: TextStyle(
                                            fontSize: 25,
                                            fontFamily: 'Lato Light',
                                            fontWeight: FontWeight.w300,
                                            color: Colors.black),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(bottom: 5),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          indicator(snapshot.data.percentajeTBD,
                                              'TBD'),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          indicator(snapshot.data.percentajeTLY,
                                              'TLY'),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          indicator(snapshot.data.percentajeTLM,
                                              'TLM'),
                                          SizedBox(
                                            width: 5,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
          Expanded(
            child: FutureBuilder(
                future: getInfoSalesDetails(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.data == null) {
                    return Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(
                                  10.0) //                 <--- border radius here
                              ),
                        ),
                        height: double.infinity,
                        width: double.infinity,
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SpinKitCircle(
                                color: Colors.white,
                                size: 40.0,
                              ),
                              JumpingText(
                                'Cargando...',
                                style: TextStyle(
                                    fontFamily: 'Lato',
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12),
                              ),
                            ],
                          ),
                        ));
                  } else if (snapshot.data == null) {
                    return Container(
                      child: Center(
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                            Icon(
                              Icons.error,
                              size: 90,
                              color: Colors.white38,
                            ),
                            Text(
                              "INFORMACIÓN NO DISPONIBLE",
                              style: TextStyle(
                                  fontSize: 12,
                                  fontFamily: 'Lato',
                                  fontWeight: FontWeight.w100,
                                  color: Colors.white),
                            ),
                            Text(
                              "¿Problemas?",
                              style: TextStyle(
                                  fontSize: 11,
                                  fontFamily: 'Lato',
                                  fontWeight: FontWeight.w100,
                                  color: Colors.white),
                            ),
                            Text(
                              "sit@prigo.com.mx",
                              style: TextStyle(
                                  fontSize: 11,
                                  fontFamily: 'Lato',
                                  fontWeight: FontWeight.w100,
                                  color: Colors.white),
                            )
                          ])),
                    );
                  } else if (snapshot.data == null) {
                    return Container(
                      child: Center(
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                            Icon(
                              Icons.error,
                              size: 90,
                              color: Colors.white38,
                            ),
                            Text(
                              "SIN CONEXIÓN A INTERNET",
                              style: TextStyle(
                                  fontSize: 12,
                                  fontFamily: 'Lato',
                                  fontWeight: FontWeight.w100,
                                  color: Colors.white),
                            ),
                            Text(
                              "Habilita tus datos móviles o red Wi-Fi ",
                              style: TextStyle(
                                  fontSize: 11,
                                  fontFamily: 'Lato',
                                  fontWeight: FontWeight.w100,
                                  color: Colors.white),
                            ),
                          ]),
                          ),
                    );
                  } else {
                    return Container(
                      padding: EdgeInsets.only(bottom:30),
                      width: MediaQuery.of(context).size.width - 40,
                      color: Colors.transparent,
                      child: ListView(
                        scrollDirection: Axis.vertical,
                          children: <Widget>[
                            SizedBox(
                              height: widthCard+25,
                              child: ListView(
                                scrollDirection: Axis.horizontal,
                                children: <Widget>[
                                  Container(
                                    child: Card(
                                      color: Colors.transparent,
                                      // elevation: 30.0,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(bottom: 5),
                                            child: Icon(
                                              AntDesign.barchart,
                                              size: 35.0,
                                              color: Colors.red,
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  left: 13,
                                                  bottom: 2,
                                                  right: 10),
                                              child: Text(
                                                "Cheque promedio",
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.white54,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding:
                                                  EdgeInsets.only(left: 13),
                                              child: Text(
                                                "\$" + snapshot.data.promChecks,
                                                style: TextStyle(
                                                  fontSize: 13,
                                                  color: Colors.white,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    width: widthCard,
                                  ),
                                  Container(
                                    child: Card(
                                      color: Colors.transparent,
                                      // elevation: 30.0,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(bottom: 5),
                                            child: Icon(
                                              AntDesign.user,
                                              size: 30.0,
                                              color: Colors.red,
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  left: 13, bottom: 2),
                                              child: Text(
                                                "Núm. comensales",
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.white54,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding:
                                                  EdgeInsets.only(left: 13),
                                              child: Text(
                                                snapshot.data.totalGuests,
                                                style: TextStyle(
                                                  fontSize: 13,
                                                  color: Colors.white,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    width: widthCard,
                                  ),
                                  Container(
                                    child: Card(
                                      color: Colors.transparent,
                                      // elevation: 30.0,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(bottom: 5),
                                            child: Icon(
                                              AntDesign.linechart,
                                              size: 30.0,
                                              color: Colors.red,
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  left: 13, bottom: 2),
                                              child: Text(
                                                "Forecast",
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.white54,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  left: 13, right: 10),
                                              child: Text(
                                                "\$" + snapshot.data.forecast,
                                                style: TextStyle(
                                                  fontSize: 13,
                                                  color: Colors.white,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    width: widthCard,
                                  ),
                                ],
                              ),
                            ),
                            Divider(
                                height: 20,
                                thickness: 0.7,
                                indent: 15,
                                endIndent: 150,
                                color: Colors.white.withOpacity(0.4)),
                            SizedBox(
                              height: widthCard+25,
                              child: ListView(
                                scrollDirection: Axis.horizontal,
                                children: <Widget>[
                                  Container(
                                    child: Card(
                                      color: Colors.transparent,
                                      // elevation: 30.0,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(bottom: 5),
                                            child: Icon(
                                              MdiIcons.tableChair,
                                              size: 35.0,
                                              color: Colors.red,
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  left: 13,
                                                  bottom: 2,
                                                  right: 10),
                                              child: Text(
                                                "Salón",
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.white54,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding:
                                                  EdgeInsets.only(left: 13),
                                              child: Text(
                                                "\$" + snapshot.data.totalSal,
                                                style: TextStyle(
                                                  fontSize: 13,
                                                  color: Colors.white,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    width: widthCard,
                                  ),
                                  Container(
                                    child: Card(
                                      color: Colors.transparent,
                                      // elevation: 30.0,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(bottom: 5),
                                            child: Icon(
                                              AntDesign.rest,
                                              size: 35.0,
                                              color: Colors.red,
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  left: 13, bottom: 2),
                                              child: Text(
                                                "Vitrina",
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.white54,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding:
                                                  EdgeInsets.only(left: 13),
                                              child: Text(
                                                "\$" + snapshot.data.totalVit,
                                                style: TextStyle(
                                                  fontSize: 13,
                                                  color: Colors.white,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    width:widthCard,
                                  ),
                                  Container(
                                    child: Card(
                                      color: Colors.transparent,
                                      // elevation: 30.0,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(bottom: 5),
                                            child: Icon(
                                              MdiIcons.moped,
                                              size: 40.0,
                                              color: Colors.red,
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  left: 13, bottom: 2),
                                              child: Text(
                                                "Uber Eats",
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.white54,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  left: 13, right: 10),
                                              child: Text(
                                                "\$" + snapshot.data.totalUber,
                                                style: TextStyle(
                                                  fontSize: 13,
                                                  color: Colors.white,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    width: widthCard,
                                  ),

                                                                    Container(
                                    child: Card(
                                      color: Colors.transparent,
                                      // elevation: 30.0,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(bottom: 5),
                                            child: Icon(
                                              MdiIcons.informationOutline,
                                              size: 40.0,
                                              color: Colors.red,
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  left: 13, bottom: 2),
                                              child: Text(
                                                "Otros",
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.white54,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  left: 13, right: 10),
                                              child: Text(
                                                "\$" + snapshot.data.totalOtros,
                                                style: TextStyle(
                                                  fontSize: 13,
                                                  color: Colors.white,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    width: widthCard,
                                  ),
                                  
                                  
                                ],
                              ),
                            ),
                            Divider(
                                height: 20,
                                thickness: 0.7,
                                indent: 150,
                                endIndent: 20,
                                color: Colors.white.withOpacity(0.4)),
                            SizedBox(
                              height: widthCard+25,
                              child: ListView(
                                scrollDirection: Axis.horizontal,
                                children: <Widget>[
                                  Container(
                                    child: Card(
                                      color: Colors.transparent,
                                      // elevation: 30.0,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(bottom: 5),
                                            child: Icon(
                                              AntDesign.creditcard,
                                              size: 35.0,
                                              color: Colors.red,
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  left: 13,
                                                  bottom: 2,
                                                  right: 10),
                                              child: Text(
                                                "Tarjeta credito",
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.white54,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding:
                                                  EdgeInsets.only(left: 13),
                                              child: Text(
                                                "\$" +
                                                    snapshot.data
                                                        .totalTenderCreditCard,
                                                style: TextStyle(
                                                  fontSize: 13,
                                                  color: Colors.white,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  left: 13,
                                                  bottom: 2,
                                                  right: 10),
                                              child: Text(
                                                "Tarjeta debito",
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.white54,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding:
                                                  EdgeInsets.only(left: 13),
                                              child: Text(
                                                "\$" +
                                                    snapshot.data
                                                        .totalTenderDebitCard,
                                                style: TextStyle(
                                                  fontSize: 13,
                                                  color: Colors.white,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    width: widthCard,
                                  ),
                                  Container(
                                    child: Card(
                                      color: Colors.transparent,
                                      // elevation: 30.0,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(bottom: 5),
                                            child: Icon(
                                              MdiIcons.cashMultiple,
                                              size: 40.0,
                                              color: Colors.red,
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  left: 13, bottom: 2),
                                              child: Text(
                                                "Efectivo",
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.white54,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding:
                                                  EdgeInsets.only(left: 13),
                                              child: Text(
                                                snapshot
                                                    .data.totalTenderCashMXN,
                                                style: TextStyle(
                                                  fontSize: 11,
                                                  color: Colors.white,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding:
                                                  EdgeInsets.only(left: 13),
                                              child: Text(
                                                snapshot
                                                    .data.totalTenderCashUSD,
                                                style: TextStyle(
                                                  fontSize: 11,
                                                  color: Colors.white,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding:
                                                  EdgeInsets.only(left: 13),
                                              child: Text(
                                                snapshot
                                                    .data.totalTenderCashEURO,
                                                style: TextStyle(
                                                  fontSize: 11,
                                                  color: Colors.white,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    width: widthCard,
                                  ),
                                  Container(
                                    child: Card(
                                      color: Colors.transparent,
                                      // elevation: 30.0,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(bottom: 5),
                                            child: Icon(
                                              MdiIcons.moped,
                                              size: 40.0,
                                              color: Colors.red,
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  left: 13, bottom: 2),
                                              child: Text(
                                                "Uber Eats:",
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.white54,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  left: 13, right: 10),
                                              child: Text(
                                                "\$" + snapshot.data.totalTenderUber,
                                                style: TextStyle(
                                                  fontSize: 13,
                                                  color: Colors.white,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    width: widthCard,
                                  ),
                                  Container(
                                    child: Card(
                                      color: Colors.transparent,
                                      // elevation: 30.0,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(bottom: 5),
                                            child: Icon(
                                              MdiIcons.informationOutline,
                                              size: 35.0,
                                              color: Colors.red,
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  left: 13, bottom: 2),
                                              child: Text(
                                                "Otros",
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.white54,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  left: 13, right: 10),
                                              child: Text(
                                                "\$" + snapshot.data.totalTenderOther,
                                                style: TextStyle(
                                                  fontSize: 13,
                                                  color: Colors.white,
                                                  fontFamily: 'Lato',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    width: widthCard,
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width - 40,
                              padding:
                                  EdgeInsets.only(right: 30, top: 30, left: 30),
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (context) => GlobalPage(),
                                    ),
                                  );
                                },
                                child: Container(
                                  height: 50,
                                  decoration: BoxDecoration(
                                      color: Colors.red[600],
                                      borderRadius:
                                          BorderRadius.circular(15.0)),
                                  child: Center(
                                    child: Text(
                                      'Ver detalle diario',
                                      style: TextStyle(
                                          fontFamily: 'Lato',
                                          fontSize: 15.0,
                                          fontWeight: FontWeight.w300,
                                          color: Colors.white),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ]),
                        );
                      }
                    }),
                  ),
                ]
                ),
              );
                    
                }
                }),
          ),

   
  
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pop(context);
        },
        child: Icon(Icons.arrow_back_ios),
      ),
    );
  }
}

class GlobalMontlySale {
  GlobalMontlySale(this.dateReport, this.totalSales, this.percentajeTBD,
      this.percentajeTLY, this.percentajeTLM, this.percentajeTLW);

  final String dateReport;
  final String percentajeTBD;
  final String percentajeTLM;
  final String percentajeTLW;
  final String percentajeTLY;
  final String totalSales;
}

class DetailsMonthSale {
  DetailsMonthSale(
      this.forecast,
      this.promChecks,
      this.totalGuests,
      this.totalVit,
      this.totalSal,
      this.totalUber,
      this.totalOtros,
      this.totalTenderCreditCard,
      this.totalTenderDebitCard,
      this.totalTenderCashMXN,
      this.totalTenderCashUSD,
      this.totalTenderCashEURO,
      this.totalTenderUber,
      this.totalTenderOther);

  final String forecast;
  final String promChecks;
  final String totalGuests;
  final String totalSal;
  final String totalTenderCashEURO;
  final String totalTenderCashMXN;
  final String totalTenderCashUSD;
  final String totalTenderCreditCard;
  final String totalTenderDebitCard;
  final String totalTenderOther;
  final String totalTenderUber;
  final String totalUber;
  final String totalVit;
  final String totalOtros;
}
